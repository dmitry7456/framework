//
//  ViewController.m
//  testFrameworkImport
//
//  Created by user on 18.02.14.
//  Copyright (c) 2014 gaincode. All rights reserved.
//

#import "ViewController.h"
#import <FI/FI.h>
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
  FIClassA *myFI = [FIClassA new];
  [myFI methodA];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
